#!/bin/bash

function usage {
  echo "\
Usage: $(basename $0) [--go] </path/to/project-name>

This script requires one argument, the path to project root. The last component
of the path will be the name of the project. For example, if the path is
~/projects/my-cool-app then the project name will be 'my-cool-app'

By default an Angular client app and NestJS server app will be generated.

If the optional --go flag is given, then a Go Gin server app will be generated
instead of the default NestJS app.
"
  exit 1
}

if [[ $# -lt 1 || $1 == '-h' || $1 == '--help' ]]; then
  usage
fi

# Check if --go flag was given
case $1 in
  -*|--*)
    if [ "$1" != "--go" ]; then
      echo "Bad arg: $1"
      echo
      usage
    else
      readonly GOAPP=1
      shift
    fi
esac


# Define some constants
readonly SCRIPT_DIR=$(readlink -f $(dirname $0))
readonly PROJ_ROOT=$(readlink -f $1)
readonly PROJ_NAME=$(basename "${PROJ_ROOT}")
readonly CONFIG_DIR="${PROJ_ROOT}"/config
readonly DOCKER_DIR="${PROJ_ROOT}"/docker
readonly CLIENT_DIR="${PROJ_ROOT}"/client
readonly SERVER_DIR="${PROJ_ROOT}"/server
if [ -n "${GOAPP}" ]; then
  # Create a project GOPATH
  readonly GOPATH="${SERVER_DIR}"/go
  readonly GOPROJ="${GOPATH}"/src/${PROJ_NAME}
fi
readonly CLIENT_IMAGE=${PROJ_NAME}-client:latest
readonly SERVER_IMAGE=${PROJ_NAME}-server:latest

# Make sure projects parent directory exists
if [ -z "${PROJ_ROOT}" ]; then
  echo "Project root directory does not exist"
  echo "To create it run: mkdir -p $1"
  exit 1
fi

# Validate project name
if [ -z "${PROJ_NAME}" ]; then
  echo "Project name cannot be empty"
  exit 1
elif [[ "${PROJ_NAME}" =~ [^a-zA-Z0-9_-] ]]; then
  echo "Project name has can only be alphanumeric in addition to '_' and '-'"
  exit 1
fi


# Check if location already exists
if [ -d "${PROJ_ROOT}" ]; then
  echo "Directory exists: ${PROJ_ROOT}"
  read -p "Remove [Y/n]? "
  if [[ -n "${REPLY}" && "${REPLY,,}" != 'y' ]]; then
    exit 1
  fi
  rm -rf "${PROJ_ROOT}" || exit 1
fi


# Create project directory structure
mkdir -p "${CONFIG_DIR}" || exit 1
mkdir -p "${DOCKER_DIR}" || exit 1
mkdir -p "${CLIENT_DIR}" || exit 1
mkdir -p "${SERVER_DIR}" || exit 1
if [ -n "${GOAPP}" ]; then
  mkdir -p "${GOPATH}"
  mkdir -p "${GOPATH}"/pkg
  mkdir -p "${GOPATH}"/src
  mkdir -p "${GOPROJ}"
fi

#------------------------------------------------------------------------------

# Copy config files to project
cp -a "${SCRIPT_DIR}"/config/*  "${CONFIG_DIR}" || exit 1
cp -a "${SCRIPT_DIR}"/docker/* "${DOCKER_DIR}" || exit 1
cp -a "${SCRIPT_DIR}"/scripts/* "${PROJ_ROOT}" || exit 1
if [ -z "${GOAPP}" ]; then
  rm "${DOCKER_DIR}"/Dockerfile.goserver || exit 1
  rm "${DOCKER_DIR}"/docker-compose.go* || exit 1
else
  mv "${DOCKER_DIR}"/Dockerfile.goserver "${DOCKER_DIR}"/Dockerfile.server || exit 1
  mv "${DOCKER_DIR}"/docker-compose.go.yml "${DOCKER_DIR}"/docker-compose.yml || exit 1
  mv "${DOCKER_DIR}"/docker-compose.go.dev.yml "${DOCKER_DIR}"/docker-compose.dev.yml || exit 1
  cp "${SCRIPT_DIR}"/goserver/main.go "${GOPROJ}"
fi

# Update variables in config files
for f in $(find "${PROJ_ROOT}" -type f); do
  sed -i "s/<PROJ_NAME>/${PROJ_NAME}/g" "${f}" || exit 1
done

#------------------------------------------------------------------------------

# Create docker images
docker pull nginx:alpine || exit 1
docker pull node:10-alpine || exit 1
if [ -n "${GOAPP}" ]; then
  docker pull golang:alpine || exit 1
fi
docker build -t ${CLIENT_IMAGE} -f "${DOCKER_DIR}"/Dockerfile.client   "${DOCKER_DIR}" || exit 1
docker build -t ${SERVER_IMAGE} -f "${DOCKER_DIR}"/Dockerfile.server   "${DOCKER_DIR}" || exit 1

#------------------------------------------------------------------------------

# Init client
FLAGS="-it --rm -v ${CLIENT_DIR}:/app"
docker run ${FLAGS} ${CLIENT_IMAGE} ng new --style=scss --routing ${PROJ_NAME} || exit 1

# After client has been initialized, we want to mount the created directory to /app
FLAGS="-it --rm -v ${CLIENT_DIR}/${PROJ_NAME}:/app"

# Install angular material
NPM_PACKAGES="@angular/flex-layout hammerjs"
docker run ${FLAGS} ${CLIENT_IMAGE} ng add @angular/material || exit 1
docker run ${FLAGS} ${CLIENT_IMAGE} npm install --save ${NPM_PACKAGES} || exit 1

# Build app to make first time stack startup faster
docker run ${FLAGS} ${CLIENT_IMAGE} ng build || exit 1

#------------------------------------------------------------------------------

# Init server
if [ -z "${GOAPP}" ]; then
  FLAGS="-it --rm -v ${SERVER_DIR}:/app"
  docker run ${FLAGS} ${SERVER_IMAGE} nest new ${PROJ_NAME} || exit 1

  # Modify default webpack.config.js
  sed -i \
"/extensions:/ a\\
    modules: [\\
        path.resolve('./src'),\\
        path.resolve('./node_modules'),\\
    ],\
" "${SERVER_DIR}"/${PROJ_NAME}/webpack.config.js || exit 1
else
  # For go projects, we install project dependencies to the project GOPATH
  FLAGS="-it --rm -v ${SERVER_DIR}/go:/go -u $(id -u):$(id -g)"
  docker run ${FLAGS} ${SERVER_IMAGE} go get -u github.com/gin-gonic/gin || exit 1
fi

exit 0

