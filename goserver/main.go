package main

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Hero struct {
	ID   uint64 `json:"id"`
	Name string `json:"name"`
}

var heroes = []Hero{
	{11, "Mr. Nice"},
	{12, "Narco"},
	{13, "Bombasto"},
	{14, "Celeritas"},
	{15, "Magneta"},
	{16, "RubberMan"},
	{17, "Dynama"},
	{18, "Dr IQ"},
	{19, "Magma"},
	{20, "Tornado"},
}

func main() {
	router := gin.Default()

	v1 := router.Group("/v1")
	{
		v1.GET("/", getRoot)
		v1.GET("/heroes", getHeroes)
		v1.GET("/hero/:id", getHero)
	}

	router.Run(":3000")
}

func getRoot(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"data": "Hello, Ginners!"})
}

func getHeroes(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"data": heroes})
}

func getHero(c *gin.Context) {
	id, err := strconv.ParseUint(c.Param("id"), 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"data": "Invalid Hero ID"})
		return
	}

	for _, hero := range heroes {
		if hero.ID == id {
			c.JSON(http.StatusOK, gin.H{"data": hero})
			return
		}
	}

	c.JSON(http.StatusBadRequest, gin.H{"data": "Invalid Hero ID"})
}
