# Angular Project Starter

The purpose of this project is to provide a script, `setup.bash`, that will
create a new web app project that uses Angular (https://angular.io) for the
client side application and NestJS (https://docs.nestjs.com) for the server
side application. There is also support to crete a Golang server side
application using Gin (https://gin-gonic.github.io/gin/) instead of NestJS.

`setup.bash`, along with the various template files, will create a new project
on your local machine that uses docker containers to build and run everything
using docker swarm.

**Note:** This was developed and tested on CentOS 7

## Create Project

```bash
$ bash setup.bash [--go] /path/to/project-name
```

This will:
- Create the projects directory structure
- Create the docker images
- Initialize the client code
  - Installs @angular/cli @angular/material @angular/cdk @angular/animations
    @angular/flex-layout and hammerjs
  - Will build the app to make first time swarm start up faster
- Initialize the server code
  - You will be prompted to answer a couple of questions during the
    initialization of NestJS
  - If the "--go" flag is given, then a Golang application will be made instead

### Library Links

Some helpful reading material

| Library | Link |
| ------- | ---- |
| Angular | https://angular.io, https://angular.io/tutorial |
| Angular CLI | https://github.com/angular/angular-cli/wiki |
| Angular Material | https://material.angular.io/guide/getting-started |
| Angular Flex Layout | https://github.com/angular/flex-layout/wiki |
| NestJS | https://docs.nestjs.com/ |
| Gin | https://gin-gonic.github.io/gin/ |

## Initialize the Docker Swarm

Run the following to initialize the Docker Swarm

```bash
$ docker swarm init
```

If you receive this error

```bash
Error response from daemon: This node is already part of a swarm. Use "docker swarm leave" to leave this swarm and join another one.
```

Then your local machine is already a member of a swarm and everything is good to
go.

## Starting the Docker Stack

Do the following to start the Docker Stack. The name of the stack will be the project name.

```bash
$ cd /path/to/project-name
$ bash app.bash start
```

or for a live reload for Angular app

```bash
$ cd /path/to/project-name
$ bash app.bash start-dev
```

### Bring the Docker Stack Down

```bash
$ cd /path/to/project-name
$ bash app.bash stop
```

## Verify App is Working

**Note:** The IP address used below may not reflect your Docker Swarm setup. To
find out what your Docker Swarm entry point is, run:

```bash
ip a show dev docker_gwbridge
```

### All Containers Running

The following commands can be used to check the status of the stack and running
containers. It will take a while for everything to be up and running. Be
patient.

```bash
$ docker ps
$ docker service ls
$ docker stack ls
$ docker stack ps app
```

### Angular

Browse to http://172.18.0.1 and see the welcome page for Angular.

### NestJS

Browse to http://172.18.0.1/api and you should see "Hello, World!", if not try
http://172.18.0.1:3000. If the former doesn't work and the later does, then
there is an issue with Nginx reverse proxy configuration.

### Gin
Browse to http://172.18.0.1/api/v1/ and you should see "Hello, Ginner!", if not
then try http://172.18.0.1:3000/v1/. If the former doesn't work and the later
does, then there is an issue with Nginx reverse proxy configuration.

### Problems

One issue might be SELinux and incorrect file types for the newly generated
files. One way around this is to `sudo setenforce 0` to disable SELinux until next
reboot. Another, more permanent and correct way, is to let SELinux know that
these newly created files need to have the `container_file_t` type so that
docker can read and/or write to them. To do this type:

```bash
$ sudo semanage fcontext -a -t container_file_t "/path/to/project-name(/.*)?"
$ sudo restorecon -R /path/to/project-name
```

I have also (rarely) encountered issues with the internal DNS provided by the
Docker Swarm not resolving the hosts correctly on all running containers. To
fix this I have added a 5 second delay in starting the nginx container.

