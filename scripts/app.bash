#!/bin/bash

readonly PROJ_DIR=$(dirname $(readlink -f $0))
readonly PROJ_NAME=$(basename "${PROJ_DIR}")
readonly CLIENT_DIR="${PROJ_DIR}"/client
readonly CONFIG_DIR="${PROJ_DIR}"/config
readonly DOCKER_DIR="${PROJ_DIR}"/docker
readonly SERVER_DIR="${PROJ_DIR}"/server
readonly CLIENT_IMAGE=${PROJ_NAME}-client
readonly SERVER_IMAGE=${PROJ_NAME}-server
readonly CLIENT_FILE="${DOCKER_DIR}"/Dockerfile.client
readonly SERVER_FILE="${DOCKER_DIR}"/Dockerfile.server
if grep -q '^FROM golang' "${SERVER_FILE}"; then
  readonly GOAPP=1
fi

case $1 in
  start)
    docker stack deploy -c "${DOCKER_DIR}"/docker-compose.yml ${PROJ_NAME}
    ;;

  start-dev)
    docker stack deploy -c "${DOCKER_DIR}"/docker-compose.dev.yml ${PROJ_NAME}
    ;;

  stop)
    docker stack rm ${PROJ_NAME}
    ;;

  docker-rebuild)
    docker pull nginx:alpine || exit 1
    docker pull node:10-alpine || exit 1
    if [ -n "${GOAPP}" ]; then
      docker pull golang:alpine || exit 1
    fi
    docker build --no-cache -t ${CLIENT_IMAGE} -f "${CLIENT_FILE}" "${DOCKER_DIR}" || exit 1
    docker build --no-cache -t ${SERVER_IMAGE} -f "${SERVER_FILE}" "${DOCKER_DIR}" || exit 1
    ;;

  client-runit)
    docker run --rm -it -v "${CLIENT_DIR}/${PROJ_NAME}":/app ${CLIENT_IMAGE} sh
    ;;

  server-runit)
    if [ -n "${GOAPP}" ]; then
      docker run --rm -it -v "${SERVER_DIR}/go/src":/go/src -v "${SERVER_DIR}/go/pkg":/go/pkg ${SERVER_IMAGE} sh
    else
      docker run --rm -it -v "${SERVER_DIR}/${PROJ_NAME}":/app ${SERVER_IMAGE} sh
    fi
    ;;

  *)
    echo "\
Usage: $(basename $0) <option>

Where option is one of:
  start             Run the regular docker stack
  start-dev         Run the docker stack with live client reload enabled
  stop              Stops the running docker stack
  docker-rebuild    Rebuild all docker images needed by this project, therby
                    updating the libraries contained therein
  client-runit      Starts the client container in interactive mode allowing
                    one to update npm packages
  server-runit      Starts the server container in interactive mode allowing
                    one to update go packages
"
    exit 1
    ;;
esac

exit 0

